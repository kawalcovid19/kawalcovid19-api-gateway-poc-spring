# kawalcovid19-api-gateway

`kawalcovid19-api-gateway` is the API gateway for [kawalcovid19.id].

## Proxy Configuration

_Note: Currently only `GET` request with text-based payload are supported._

To configure proxy and routing, start the application with the `-Dproxy.*.host=?` args or `PROXY_*_HOST=?` environment variable. For example:

  - `PROXY_LICENCES_HOST=https://licenseapi.herokuapp.com/licenses/` will route `GET` requests from `/api/proxy/licences/0bsd` to `https://licenseapi.herokuapp.com/licenses/0bsd`.
  - `-Dproxy.publicapis.host=https://api.publicapis.org/` will route `GET` requests from `/api/proxy/publicapis/entries` to `https://api.publicapis.org/entries`.

By default, requests are cached based on their destination URLs (ignoring header values). Default cache TTL is one hour.

## Prerequisites

The following tools are required to work with this project:

  - [Java SDK] 11

If you are using Linux or macOS, [sdkman] is a useful tool to install the requirements above.

## Setup Steps

The application can be built and run via different methods:

- Gradle for development, and
- All-in-one JAR for distribution 

Following any of the above method will allow the application to be accessible from [`localhost:8080`](http://localhost:8080) from your browser.

### Development mode via Gradle

  1. Run `./gradlew bootRun` to start the application in development mode

### All-in-One JAR

  1. Run `./gradlew bootJar` to build an all-in-one JAR for the web application
  2. Run `java -jar build/libs/*-all.jar` to run the application

Now you can visit it on [`localhost:8080`](http://localhost:8080) from your browser.

### Source Code Formatting

We use [scalafmt] for code-formatting. Ensure to setup your IDE to apply provided `.scalafmt.conf` rules, or alternatively:

  - Run `./gradlew spotlessApply` to apply formatting to the codebase

Currently [scalafmt] does not provide optimise imports capability, so ensure the following import order is set within your IDE of choice, for consistency:

  1. `java`
  2. `javax`
  3. `scala`
  4. _blank line / separator_
  5. _all other packages_
  6. _blank line / separator_
  7. `id.kawalcovid19`

## Contributing

We follow the "[feature-branch]" Git workflow.

  1. Commit changes to a branch in your fork (use `snake_case` convention):
     - For technical chores, use `chore/` prefix followed by the short description, e.g. `chore/do_this_chore`
     - For new features, use `feature/` prefix followed by the feature name, e.g. `feature/feature_name`
     - For bug fixes, use `bug/` prefix followed by the short description, e.g. `bug/fix_this_bug`
  2. Rebase or merge from "upstream"
  3. Submit a PR "upstream" to `develop` branch with your changes

Please read [CONTRIBUTING] for more details.

## License

```
  MIT License

  Copyright (c) 2020 kawalcovid19.id contributors

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
```

`kawalcovid19-api-gateway` is released under the MIT License. See the [LICENSE] file for further details.

[CONTRIBUTING]: https://gitlab.com/kawalcovid19/kawalcovid19-api-gateway/-/blob/master/CONTRIBUTING.md
[feature-branch]: http://nvie.com/posts/a-successful-git-branching-model/
[Java SDK]: https://adoptopenjdk.net/
[kawalcovid19.id]: https://kawalcovid19.id/
[LICENSE]: https://gitlab.com/kawalcovid19/kawalcovid19-api-gateway/-/blob/master/LICENSE.txt
[scalafmt]: https://scalameta.org/scalafmt/
[sdkman]: https://sdkman.io/

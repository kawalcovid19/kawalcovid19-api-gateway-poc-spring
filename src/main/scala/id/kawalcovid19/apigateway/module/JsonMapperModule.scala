package id.kawalcovid19.apigateway.module

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.springframework.context.annotation.{Bean, Configuration}
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder

@Configuration
class JsonMapperModule {

  @Bean
  def objectMapper(builder: Jackson2ObjectMapperBuilder): ObjectMapper = {
    val modules = Seq(
      new DefaultScalaModule,
      new JavaTimeModule,
      new Jdk8Module
    )

    builder
      .modules(modules: _*)
      .build()
  }
}

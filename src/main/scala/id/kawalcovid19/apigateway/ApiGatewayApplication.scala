package id.kawalcovid19.apigateway

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Import

import id.kawalcovid19.apigateway.module.JsonMapperModule

@Import(
  value = Array(
    classOf[JsonMapperModule]
  )
)
@SpringBootApplication
class ApiGatewayApplication {}

object ApiGatewayApplication {

  def main(args: Array[String]): Unit = {
    SpringApplication.run(classOf[ApiGatewayApplication], args: _*)
    ()
  }
}

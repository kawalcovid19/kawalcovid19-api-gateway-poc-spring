package id.kawalcovid19.apigateway.controller

import scala.beans.BeanProperty

import org.springframework.web.bind.annotation.{
  GetMapping,
  RequestMapping,
  RestController
}

@RestController
@RequestMapping(path = Array("/api"))
class PingController {
  import PingController._

  @GetMapping(path = Array("/ping"))
  def ping(): PongResponse =
    PongResponse(PONG)
}

object PingController {

  private[controller] final val PONG = "pong"

  final case class PongResponse(@BeanProperty data: String)
}

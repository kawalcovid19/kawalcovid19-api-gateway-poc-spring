package id.kawalcovid19.apigateway.controller

import java.util.{Map => JMap}
import scala.jdk.javaapi.{CollectionConverters => Conv}

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.env.Environment
import org.springframework.http.{HttpStatus, ResponseEntity}
import org.springframework.util.{LinkedMultiValueMap, MultiValueMap}
import org.springframework.web.bind.annotation.{
  GetMapping,
  PathVariable,
  RequestHeader,
  RequestMapping,
  RestController
}
import scalacache.guava.GuavaCache
import sttp.client.HttpURLConnectionBackend
import sttp.model.Header

@RestController
@RequestMapping(path = Array("/api"))
class CachingProxyController @Autowired()(
    env: Environment
) {
  import CachingProxyController._

  implicit val backend = HttpURLConnectionBackend()
  implicit val guavaCache = GuavaCache[ResponseEntity[String]]

  @GetMapping(path = Array("/proxy/{key}/{*path}"))
  def proxiedGetRequest(
      @PathVariable key: String,
      @PathVariable path: String,
      @RequestHeader headers: JMap[String, String]
  ): ResponseEntity[String] = {
    import scalacache._
    import scalacache.modes.sync._

    ProxyRoute
      .from(env, key, path)
      .fold(NOT_FOUND) { route =>
        sync
          .get(route.cacheKey)
          .getOrElse(getResponse(route, Conv.asScala(headers).toMap))
      }
  }

  private case class ProxyRoute(key: String, host: String, path: String) {
    import sttp.client._

    final val absolutePath =
      uri"${host.stripSuffix("/")}/${path.stripPrefix("/")}"

    final val cacheKey =
      s"${key}:/${path.stripPrefix("/")}"
  }
  private object ProxyRoute {
    def from(
        env: Environment,
        key: String,
        path: String
    ): Option[ProxyRoute] = {
      val proxyHostKey = s"proxy.${key}.host"
      val hasProxyHost = env.containsProperty(proxyHostKey)
      if (hasProxyHost) {
        val host = env.getProperty(proxyHostKey)
        Some(ProxyRoute(key, host, path))
      } else {
        Option.empty[ProxyRoute]
      }
    }
  }

  private[this] def getResponse(
      route: ProxyRoute,
      headers: Map[String, String]
  ): ResponseEntity[String] = {
    import scalacache._
    import scalacache.modes.sync._

    val request =
      sttp.client.basicRequest
        .headers(headers)
        .get(route.absolutePath)

    val response = request.send()

    response.body.fold(
      _ => ResponseEntity.status(response.code.code).build(),
      body => {
        val headers = toMultiValueMap(response.headers)
        val httpStatus = HttpStatus.valueOf(response.code.code)
        val result = new ResponseEntity(body, headers, httpStatus)

        sync.put(route.cacheKey)(result, ttl = DEFAULT_TTL)

        result
      }
    )
  }

  private[this] def toMultiValueMap(
      headers: Seq[Header]
  ): MultiValueMap[String, String] =
    new LinkedMultiValueMap[String, String] {
      headers.foreach { h =>
        this.add(h.name, h.value)
      }
    }
}

object CachingProxyController {
  import scala.concurrent.duration._

  private[controller] final val NOT_FOUND =
    new ResponseEntity[String](HttpStatus.NOT_FOUND)

  private[controller] final val DEFAULT_TTL =
    Some(1.hour)
}

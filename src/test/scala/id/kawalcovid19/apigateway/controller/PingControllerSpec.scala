package id.kawalcovid19.apigateway.controller

import org.junit.runner.RunWith
import org.scalatest.{MustMatchers, WordSpec}
import org.scalatestplus.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class PingControllerSpec extends WordSpec with MustMatchers {
  import PingController._

  private lazy val controller = new PingController

  "Ping controller" when {

    "receiving 'ping' request" should {

      "respond with 'pong'" in {
        val response = controller.ping()

        response.data mustEqual PONG
      }
    }
  }
}
